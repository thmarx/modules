
package com.thorstenmarx.modules.api;

/*-
 * #%L
 * Modules-API
 * %%
 * Copyright (C) 2015 - 2018 Thorsten Marx
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


/**
 * The Context is passed to the module. It can be used to inject implementation dependend objects or functionality
 *
 * @author marx
 */
public abstract class Context {

	private ServiceRegistry services = null;
	
	public ServiceRegistry serviceRegistry() {
		return services;
	}

	public void setServices(ServiceRegistry services) {
		this.services = services;
	}
	
	
}
