module serviceModule {
	requires service.api;
	requires com.thorstenmarx.modules.api;

	provides com.thorstenmarx.modules.serviceapi.MyService 
			with com.thorstenmarx.modules.servicemodule.MyServiceImpl;
			
	provides com.thorstenmarx.modules.api.ModuleLifeCycleExtension 
			with com.thorstenmarx.modules.servicemodule.ServiceRegistration;
}
