/**
 * ModuleManager
 * Copyright (C) 2016  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.thorstenmarx.modules.testmodule;

import com.thorstenmarx.modules.serviceapi.MyService;
import com.thorstenmarx.modules.testapi.api.TestExtension;
import java.io.IOException;
//import org.apache.commons.math3.distribution.TDistribution;
import com.thorstenmarx.modules.api.annotation.Extension;
import com.thorstenmarx.modules.testapi.api.TestService;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;

/**
 *
 * @author thmarx
 */
@Extension(TestExtension.class)
public class TestImple extends TestExtension {

	@Inject
	TestService testService;
	
	@Override
	public String getName() {

		configuration.set("test", "das ist der hammer");
		try {
			configuration.store();
		} catch (IOException ex) {
			Logger.getLogger(TestImple.class.getName()).log(Level.SEVERE, null, ex);
		}

//		TDistribution t = new TDistribution(29);
//		double lowerTail = t.cumulativeProbability(-2.656);

		Optional<MyService> optional = getContext().serviceRegistry().single(MyService.class);
		MyService ms = optional.get();

		System.out.println(getContext().getMyName("test module"));

		System.out.println("aobject title: " + ms.getAObject().getTitle());
		
		System.out.println("TestService: " + testService.getServiceName());

		final String lowerTail = " eine Nachricht ";
		
		return "ich bins: " + lowerTail + " from service: " + ms.sayHello("thorsten");
	}

	@Override
	public void init() {
	}

}
