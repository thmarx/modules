module testModule {
	requires test.api;
	requires service.api;
	requires com.thorstenmarx.modules.api;

	requires javax.inject;
	requires java.logging;

	provides com.thorstenmarx.modules.testapi.api.TestExtension
			with com.thorstenmarx.modules.testmodule.TestImple;
	
	uses com.thorstenmarx.modules.serviceapi.MyService;
	
	requires java.compiler;
	uses javax.annotation.processing.Processor;
}

