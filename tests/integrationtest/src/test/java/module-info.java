module IntegrationTest {
	requires testng;
	requires assertj.core;
	requires com.google.guice;
	requires test.api;
	requires service.api;
	requires com.thorstenmarx.modules.api;
	requires com.thorstenmarx.modules.manager;
	
	uses com.thorstenmarx.modules.testapi.api.TestService;
	uses com.thorstenmarx.modules.serviceapi.MyService;
}
