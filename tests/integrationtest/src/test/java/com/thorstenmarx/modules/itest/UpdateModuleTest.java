/**
 * ModuleManager
 * Copyright (C) 2016  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.thorstenmarx.modules.itest;


import com.thorstenmarx.modules.testapi.api.TestExtension;
import com.thorstenmarx.modules.testapi.api.TestContext;
import com.thorstenmarx.modules.ModuleAPIClassLoader;
import com.thorstenmarx.modules.ModuleManagerImpl;
import com.thorstenmarx.modules.api.ModuleDescription;
import com.thorstenmarx.modules.api.ModuleManager;
import com.thorstenmarx.modules.itest.module.GuiceModuleInjector;
import com.thorstenmarx.modules.serviceapi.MyService;
import java.io.File;
import java.io.IOException;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Arrays;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author marx
 */
public class UpdateModuleTest {
	
	ModuleManager manager;
	private TestContext context;
	
	@BeforeClass
	public void manager_init () {
		List<String> apiPackages = new ArrayList<>();
		context = new TestContext();
		apiPackages.add("com.thorstenmarx.modules.testapi.api");
		manager = ModuleManagerImpl.create(new File("../example/"), context, new ModuleAPIClassLoader(new URLClassLoader(Arrays.array(), getClass().getClassLoader()), apiPackages), new GuiceModuleInjector());
	}
	@AfterClass(alwaysRun = true)
	public void manager_close () throws IOException {
		manager.deactivateModule("test");
		manager.deactivateModule("service");
		manager.uninstallModule("test", true);
		manager.uninstallModule("service", true);
		((ModuleManagerImpl)manager).close();
	}
	
	@Test()
	public void module_not_loaded () {
		Assertions.assertThat(manager.module("test")).isNull();
	}
	@Test(dependsOnMethods = "module_not_loaded", groups = "service")
	public void install_service_module () throws Exception {
		var moduleFile = new File("../example/toinstall/service-module.zip");
		final var moduleID = manager.installModule(moduleFile.toURI());
		System.out.println("installed: " + moduleID);
		manager.activateModule(moduleID);
	}
	
	@Test(dependsOnMethods = "install_service_module", groups = "service")
	public void update_serivce_module () throws Exception {
		var moduleFile = new File("../example/toinstall/service-module.zip");
		final var moduleID = manager.installModule(moduleFile.toURI());
		System.out.println("installed: " + moduleID);
	}
}
