/**
 * ModuleManager
 * Copyright (C) 2016  ThorstenMarx (kontakt@thorstenmarx.com)
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.thorstenmarx.modules.itest;


import com.thorstenmarx.modules.testapi.api.TestExtension;
import com.thorstenmarx.modules.testapi.api.TestContext;
import com.thorstenmarx.modules.ModuleAPIClassLoader;
import com.thorstenmarx.modules.ModuleManagerImpl;
import com.thorstenmarx.modules.api.ModuleDescription;
import com.thorstenmarx.modules.api.ModuleManager;
import com.thorstenmarx.modules.itest.module.GuiceModuleInjector;
import com.thorstenmarx.modules.serviceapi.MyService;
import java.io.File;
import java.io.IOException;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Arrays;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 *
 * @author marx
 */
public class ModuleManagerTest {
	
	ModuleManager manager;
	private TestContext context;
	
	@BeforeClass
	public void manager_init () {
		List<String> apiPackages = new ArrayList<>();
		context = new TestContext();
		apiPackages.add("com.thorstenmarx.modules.testapi.api");
		manager = ModuleManagerImpl.create(new File("../example/"), context, new ModuleAPIClassLoader(new URLClassLoader(Arrays.array(), getClass().getClassLoader()), apiPackages), new GuiceModuleInjector());
	}
	@AfterClass(alwaysRun = true)
	public void manager_close () throws IOException {
		manager.deactivateModule("test");
		manager.deactivateModule("service");
		manager.uninstallModule("test", true);
		manager.uninstallModule("service", true);
		((ModuleManagerImpl)manager).close();
	}
	
	@Test()
	public void module_not_loaded () {
		Assertions.assertThat(manager.module("test")).isNull();
	}
	@Test(dependsOnMethods = "module_not_loaded", groups = "service")
	public void install_service_module () throws Exception {
		var moduleFile = new File("../example/toinstall/service-module.zip");
		final var moduleID = manager.installModule(moduleFile.toURI());
		System.out.println("installed: " + moduleID);
	}
	
	@Test(dependsOnMethods = "install_service_module", groups = "service")
	public void description_of_unloaded_module () throws Exception {
		ModuleDescription description = manager.description("service");
		Assertions.assertThat(description).isNotNull();
		Assertions.assertThat(description.getName()).isEqualTo("Service Module");
		Assertions.assertThat(description.getVersion()).isEqualTo("1");
	}
	
	@Test(dependsOnMethods = "description_of_unloaded_module", groups = "service")
	public void activate_service_module () throws IOException {
		var result = manager.activateModule("service");
		Assertions.assertThat(result).isTrue();
	}
	
	@Test(dependsOnMethods = "activate_service_module", groups = "service")
	public void test_service_module_myservice () throws IOException {
		var service = context.serviceRegistry().single(MyService.class);
		Assertions.assertThat(service).isPresent();
		System.out.println(service.get().sayHello("Thorsten"));
	}
	
	@Test(dependsOnGroups = "service")
	public void install_test_module () throws Exception {
		var moduleFile = new File("../example/toinstall/test-module.zip");
		final var moduleID = manager.installModule(moduleFile.toURI());
		var active = manager.activateModule(moduleID);
		
		Assertions.assertThat(active).isTrue();
	}
	
	@Test(dependsOnMethods = "install_test_module")
	public void module_provides_extension () {
		var module = manager.module("test");
		Assertions.assertThat(module.provides(TestExtension.class)).isTrue();
		
		var name = module.extensions(TestExtension.class).get(0).getName();
		System.out.println(name);
	}
//	
//	@Test(dependsOnMethods = "module_provides_extension")
//	public void module_provided_extension_returns_value () {
//		Module module = manager.module("test");
//		List<TestExtension> teExt = module.extensions(TestExtension.class);
//		Assertions.assertThat(teExt).isNotNull().hasSize(1);
//		Assertions.assertThat(teExt.get(0).getName()).isNotNull();
//		System.out.println(teExt.get(0).getName());
//	}
//	
//	@Test(dependsOnMethods = "module_provided_extension_returns_value")
//	public void module_deactivation () throws IOException {
//		manager.deactivateModule("test");
//		Assertions.assertThat(manager.module("test")).as("deactivating of module not working").isNull();
//	}
//	@Test(dependsOnMethods = "module_deactivation")
//	public void module_activation () throws IOException {
//		manager.activateModule("test");
//		Assertions.assertThat(manager.module("test")).isNotNull().as("activating of module not working");
//	}
	
//	@Test(dependsOnMethods = "module_activation")
//	public void module_install () throws IOException {
//		File moduleFile = new File("src/test/resources/demo-module-1.4.0.zip");
//		final String moduleID = manager.installModule(moduleFile.toURI());
//		
//		Assertions.assertThat(manager.module(moduleID)).as("module should be null after installation").isNull();
//		manager.activateModule(moduleID);
//		Assertions.assertThat(manager.module(moduleID)).as("installed module could not be activated").isNotNull();
//		
//		Module module = manager.module(moduleID);
//		List<TestExtension> teExt = module.extensions(TestExtension.class);
//		Assertions.assertThat(teExt).isNotNull().hasSize(3);
//		Assertions.assertThat(teExt.get(0).getName()).isNotNull();
//		System.out.println(teExt.get(0).getName());
//		
//		teExt = null;
//		module = null;
//		System.gc();
//	}
//	
//	@Test(dependsOnMethods = "module_install", expectedExceptions = {NoClassDefFoundError.class})
//	public void module_test_api () throws IOException {
//		Module module = manager.module("demo-module");
//		List<TestExtension> teExt = module.extensions(TestExtension.class);
//		
//		System.out.println(getClass().getClassLoader());
//		for (TestExtension ext : teExt) {
//			System.out.println(ext.getName());
//			System.out.println(ext.getClass().getClassLoader());
//		}
//		
//		teExt = null;
//		module = null;
//		System.gc();
//	}
//	
//	
//	
//	@Test(dependsOnMethods = "module_test_api", expectedExceptions = IOException.class)
//	public void module_uninstall_activated () throws IOException {
//		manager.uninstallModule("demo-module", true);
//	}
//	@Test(dependsOnMethods = "module_uninstall_activated")
//	public void module_uninstall () throws IOException {
//		manager.deactivateModule("demo-module");
//		manager.uninstallModule("demo-module", true);
//		
//		Assertions.assertThat(manager.module("demo-module")).as("module should be null after installation").isNull();
//	}
//	@Test(dependsOnMethods = "module_uninstall")
//	public void module_manager_configuration () throws IOException {
//		ManagerConfiguration.ModuleConfig moduleConfig = manager.configuration().get("test");
//		
//		Assertions.assertThat(moduleConfig.isActive()).as("module should be active").isTrue();
//	}
}
